package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {

    @Test
    public void testEmployee() {
        // Create objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        SalesEmployee victoria = new SalesEmployee();

        // Hire hannah and fire bob
        hannah.employ();
        bob.fire();

        // employ victoria and give her a car
        victoria.employ();
        victoria.changeCar("bmw");

        // Print out to the screen
        System.out.println("Hannah employement state:" + hannah.employed);
        System.out.println("Bob employment state: " + bob.employed);
        System.out.println("Victoria employment state: " + victoria.employed);
        System.out.println("Victoria's car: " + victoria.car);

    }


}
