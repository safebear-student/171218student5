package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test
    public void LoginTest() {
        // Step 1 - Action: Open web application in the browser
        driver.get(Utils.getUrl());

        // Step 2 - Expected result: check I am on the login page
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page" , "This is not the login page OR the title has changed");

        // Step 3 - Enter valid details and click login
        loginPage.login("tester", "letmein");

        // THE FOLLOWING is another way of setting the login details
        // loginPage.enterUsername("tester");
        // loginPage.enterPassword("letmein");
        //  loginPage.clickLoginButton();

        // Step 4 - Expected result: check I am on the tools page
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "This is not the tools page OR the title has changed");
    }
}
