package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle() {

        return driver.getTitle().toString();
    }

    public void enterUsername(String username) {

        driver.findElement(locators.getUsernameFieldLocator()).sendKeys(username);
    }

    public void enterPassword(String pwd) {

        driver.findElement(locators.getPasswordFieldLocator()).sendKeys(pwd);
    }

    public void clickLoginButton(){

        driver.findElement(locators.getLoginButtonLocator()).click();
    }

    public void login(String username, String pwd){

        enterUsername(username);
        enterPassword(pwd);
        clickLoginButton();
    }
}
