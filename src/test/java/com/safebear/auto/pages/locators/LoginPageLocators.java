package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {
    private By usernameFieldLocator = By.id("username");
    private By passwordFieldLocator = By.id("password");
    private By loginButtonLocator = By.xpath(".//button[@id='enter']");



}
